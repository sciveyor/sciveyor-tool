package cmd

import (
	"codeberg.org/sciveyor/sciveyor-tool/mongo"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// mongoCmd represents the mongo command
var mongoCmd = &cobra.Command{
	Use:   "mongo",
	Short: "Work with MongoDB servers",
	Long: `This collection of commands handles working with our
internal MongoDB server, which contains our long-term
article corpus storage.`,
}

func addMongoFlags(cmd *cobra.Command) {
	cmd.Flags().String("mongo-address", "", "Address of the MongoDB server")
	cmd.Flags().String("mongo-database", "", "Database to access on the MongoDB server")
	cmd.Flags().String("mongo-collection", "", "Collection to access on the MongoDB server")
	cmd.Flags().Int("mongo-timeout", 30, "Timeout (in seconds) for MongoDB connections to complete and close")
}

func bindMongoFlags(cmd *cobra.Command) {
	viper.BindPFlag("mongo.address", cmd.Flags().Lookup("mongo-address"))
	viper.BindPFlag("mongo.database", cmd.Flags().Lookup("mongo-database"))
	viper.BindPFlag("mongo.collection", cmd.Flags().Lookup("mongo-collection"))
	viper.BindPFlag("mongo.timeout", cmd.Flags().Lookup("mongo-timeout"))
}

func getMongoFlags() *mongo.Config {
	return &mongo.Config{
		Address:    viper.GetString("mongo.address"),
		Database:   viper.GetString("mongo.database"),
		Collection: viper.GetString("mongo.collection"),
		Timeout:    viper.GetInt("mongo.timeout"),
	}
}

func init() {
	rootCmd.AddCommand(mongoCmd)
}

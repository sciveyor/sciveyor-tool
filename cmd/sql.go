package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func addSqlFlags(cmd *cobra.Command) {
	cmd.Flags().String("db-address", "", "Connection URL for the PostgreSQL server")
}

func bindSqlFlags(cmd *cobra.Command) {
	viper.BindPFlag("db.address", cmd.Flags().Lookup("db-address"))
}

func getSqlFlags() map[string]string {
	return map[string]string{
		"Address": viper.GetString("db.address"),
	}
}

package cmd

import (
	"os"

	"codeberg.org/sciveyor/sciveyor-tool/json"

	"github.com/spf13/cobra"
	"golang.org/x/term"
)

var jiBatchSize int

// jsonImportCmd represents the json validate command
var jsonImportCmd = &cobra.Command{
	Use:   "import [files or glob patterns to validate]",
	Short: "Import a collection of JSON files to MongoDB",
	Long: `Imports the given set of files to the MongoDB server.
Files may be specified individually:

sciveyor-tool json import file.json

or by glob patterns, including double-star globs:

sciveyor-tool json import './**/*.json'

The --batch-size parameter may be adjusted to speed up
traffic on the network. The default value of 100 should
work in most cases.`,
	Args: cobra.MinimumNArgs(1),
	PreRun: func(cmd *cobra.Command, args []string) {
		bindMongoFlags(cmd)
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		return json.Import(getMongoFlags(), args, json.ImportConfig{
			BatchSize:   jiBatchSize,
			Interactive: term.IsTerminal(int(os.Stderr.Fd())),
		})
	},
}

func init() {
	jsonCmd.AddCommand(jsonImportCmd)

	addMongoFlags(jsonImportCmd)
	jsonImportCmd.Flags().IntVar(&jiBatchSize, "batch-size", 100, "Number of files to send to MongoDB at one time")
}

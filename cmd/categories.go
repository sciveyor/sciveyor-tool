package cmd

import (
	"codeberg.org/sciveyor/sciveyor-tool/categories"

	"github.com/spf13/cobra"
)

// categoriesCmd represents the categories command
var categoriesCmd = &cobra.Command{
	Use:   "categories",
	Short: "Launch an interactive terminal category editor",
	Long: `Opens an editor that will allow you to create and remove
journal categories, assign or remove journals from those
categories, and perform various kinds of automatic cleanup
and maintenance operations on the categories.`,
	PreRun: func(cmd *cobra.Command, args []string) {
		bindSqlFlags(cmd)
		bindAppFlags(cmd)
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		sql := getSqlFlags()
		app := getAppFlags()

		return categories.Editor(categories.EditorConfig{
			AppAddress: app["Address"],
			DbAddress:  sql["Address"],
		})
	},
}

func init() {
	rootCmd.AddCommand(categoriesCmd)

	addSqlFlags(categoriesCmd)
	addAppFlags(categoriesCmd)
}

package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func addAppFlags(cmd *cobra.Command) {
	cmd.Flags().String("sciveyor-address", "", "Address of the Sciveyor server")
}

func bindAppFlags(cmd *cobra.Command) {
	viper.BindPFlag("sciveyor.address", cmd.Flags().Lookup("sciveyor-address"))
}

func getAppFlags() map[string]string {
	return map[string]string{
		"Address": viper.GetString("sciveyor.address"),
	}
}

package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "sciveyor-tool",
	Short: "The administration utility for Sciveyor",
	Long: `sciveyor-tool is a CLI interface to perform a variety of
administration tasks for an installation of Sciveyor. For more
information, see the help for each of the sub-commands here.`,

	Version: "0.8",
}

// Execute adds all child commands to the root command and sets flags
// appropriately. This is called by main.main(). It only needs to happen once
// to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}

	viper.WriteConfig()
}

func init() {
	cobra.OnInitialize(initConfig)

	// Global configuration flags for the entire application
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.sciveyor.yaml)")
	rootCmd.PersistentFlags().BoolP("verbose", "v", false, "print extra debugging information")
	viper.BindPFlag("verbose", rootCmd.PersistentFlags().Lookup("verbose"))
}

// initConfig reads in config file and ENV variables, if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".sciveyor"
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".sciveyor")
	}

	// Try to read environment variables and a config file
	viper.AutomaticEnv()
	viper.ReadInConfig()
}

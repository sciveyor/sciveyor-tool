package cmd

import (
	"os"

	"codeberg.org/sciveyor/sciveyor-tool/sync"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"golang.org/x/term"
)

var sBatchSize int
var sForce bool

// syncCmd represents the sync command
var syncCmd = &cobra.Command{
	Use:   "sync",
	Short: "Synchronize the MonogDB server to Solr",
	Long: `Synchronizes changes in the MongoDB database with the
Solr server. This synchronization will perform the
following operations:

- any documents added to Mongo and not present in Solr will
  be added to Solr
- any documents deleted from Mongo and present in Solr will
  be deleted from Solr
- any documents present in both Mongo and Solr, but with a
  newer version or dataSourceVersion attribute, will be
  updated (replaced) in Solr

If --force is passed, then all documents in Solr will be
replaced with the version in MongoDB.

The --batch-size parameter may be adjusted to set the
frequency with which we run a Solr commit operation.`,
	PreRun: func(cmd *cobra.Command, args []string) {
		bindMongoFlags(cmd)
		bindSolrFlags(cmd)
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		return sync.Run(getMongoFlags(), getSolrFlags(), sync.Config{
			BatchSize:   sBatchSize,
			Force:       sForce,
			Interactive: term.IsTerminal(int(os.Stderr.Fd())),
			Verbose:     viper.GetBool("verbose"),
		})
	},
}

func init() {
	rootCmd.AddCommand(syncCmd)

	addMongoFlags(syncCmd)
	addSolrFlags(syncCmd)
	syncCmd.Flags().IntVar(&sBatchSize, "batch-size", 100, "Number of operations before we send a Solr commit")
	syncCmd.Flags().BoolVarP(&sForce, "force", "f", false, "Replace all Solr documents with the version in MongoDB (rather than checking for updates)")
}

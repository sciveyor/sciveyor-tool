package cmd

import (
	"github.com/spf13/cobra"
)

// jsonCmd represents the json command
var jsonCmd = &cobra.Command{
	Use:   "json",
	Short: "Work with JSON files containing article data",
	Long: `This collection of commands handles JSON files in the
Sciveyor JSON format, which is described at:
<https://data.sciveyor.com/schema/>.`,
}

func init() {
	rootCmd.AddCommand(jsonCmd)
}

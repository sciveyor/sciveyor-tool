package cmd

import (
	"os"

	"codeberg.org/sciveyor/sciveyor-tool/json"

	"github.com/spf13/cobra"
	"golang.org/x/term"
)

var jvLoose bool
var jvUnique bool

// jsonValidateCmd represents the json validate command
var jsonValidateCmd = &cobra.Command{
	Use:   "validate [files or glob patterns to validate]",
	Short: "Validate a collection of JSON files",
	Long: `Checks the given set of files to determine whether or not
they adhere to the Sciveyor JSON schema, printing any
errors encountered.

Files may be specified individually:

sciveyor-tool json validate file.json

or by glob patterns, including double-star globs:

sciveyor-tool json validate './**/*.json'

In normal operation (strict mode), errors will be reported
for all fields with invalid data, as well as for any
"extra" fields present in the JSON files (i.e., fields
missing from the JSON schema). To disable this, pass the
--loose flag.

Optionally (because keeping track of this is slow), this
command can also ensure that the ID values across all of
the given JSON files are unique; pass --unique to enable
this behavior.`,
	Args: cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		return json.ValidateFiles(args, json.ValidateConfig{
			Strict:      !jvLoose,
			Unique:      jvUnique,
			Interactive: term.IsTerminal(int(os.Stderr.Fd())),
		})
	},
}

func init() {
	jsonCmd.AddCommand(jsonValidateCmd)

	jsonValidateCmd.Flags().BoolVar(&jvLoose, "loose", false, "Ignore fields in JSON files that are absent in the formal schema (default is false, or strict mode)")
	jsonValidateCmd.Flags().BoolVar(&jvUnique, "unique", false, "Check to see whether or not the ID values in these files are unique, print errors if there are duplicates (slower, disabled by default)")
}

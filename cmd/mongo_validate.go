package cmd

import (
	"os"

	"codeberg.org/sciveyor/sciveyor-tool/mongo"

	"github.com/spf13/cobra"
	"golang.org/x/term"
)

var mvLoose bool

// mongoValidateCmd represents the mongo validate command
var mongoValidateCmd = &cobra.Command{
	Use:   "validate",
	Short: "Validate a MongoDB server",
	Long: `Checks the content of the MongoDB server to determine
whether or not the documents contained within the given
collection adhere to the Sciveyor JSON schema, printing any
errors encountered.

In normal operation (strict mode), errors will be reported
for all documents with invalid data, as well as for any
"extra" fields present in the documents (i.e., fields
missing from the JSON schema). To disable this, pass the
--loose flag.`,
	PreRun: func(cmd *cobra.Command, args []string) {
		bindMongoFlags(cmd)
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		return mongo.Validate(getMongoFlags(), mongo.ValidateConfig{
			Strict:      !mvLoose,
			Interactive: term.IsTerminal(int(os.Stderr.Fd())),
		})
	},
}

func init() {
	mongoCmd.AddCommand(mongoValidateCmd)

	addMongoFlags(mongoValidateCmd)
	mongoValidateCmd.Flags().BoolVar(&mvLoose, "loose", false, "Ignore fields in JSON files that are absent in the formal schema (default is false, or strict mode)")
}

package cmd

import (
	"codeberg.org/sciveyor/sciveyor-tool/solr"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func addSolrFlags(cmd *cobra.Command) {
	cmd.Flags().String("solr-address", "", "Address of the Solr server")
	cmd.Flags().String("solr-collection", "", "Collection to access on the Solr server")
}

func bindSolrFlags(cmd *cobra.Command) {
	viper.BindPFlag("solr.address", cmd.Flags().Lookup("solr-address"))
	viper.BindPFlag("solr.collection", cmd.Flags().Lookup("solr-collection"))
}

func getSolrFlags() *solr.Config {
	return &solr.Config{
		Address:    viper.GetString("solr.address"),
		Collection: viper.GetString("solr.collection"),
	}
}

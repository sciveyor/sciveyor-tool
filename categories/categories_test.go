package categories

import (
	"database/sql"
	"testing"

	// Unnamed import for PostgreSQL driver
	_ "github.com/lib/pq"
)

func getTestCategories(t *testing.T) *Categories {
	connStr := "postgres://localhost/sciveyor_test?search_path=sciveyor&sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		t.Errorf("cannot connect to testing database: %v", err)
		return nil
	}

	cats := &Categories{db: db}

	list, err := cats.FindAll("")
	if err != nil {
		t.Errorf("expected to get list of all categories, failed: %v", err)
	}

	for _, cat := range list {
		err = cat.Delete()
		if err != nil {
			t.Errorf("expected to delete category, failed: %v", err)
		}
	}

	return cats
}

func TestCategories(t *testing.T) {
	cats := getTestCategories(t)

	// Add a root and a leaf
	root, err := cats.Add("root", []string{"PNTDS", "PLoS Neglected Tropical Diseases"}, []string{})
	if err != nil {
		t.Errorf("failed when adding root category: %v", err)
	}

	leaf, err := cats.Add("leaf", []string{"PNTDS"}, []string{root.ID})
	if err != nil {
		t.Errorf("failed when adding leaf category: %v", err)
	}

	t.Run("Roots", func(t *testing.T) {
		roots, err := cats.Roots()
		if err != nil {
			t.Errorf("failed when getting all roots: %v", err)
		}
		if len(roots) != 1 {
			t.Errorf("expected one root, got %v", len(roots))
		}
		if roots[0].ID != root.ID {
			t.Errorf("expected Roots() to return root id (%v), got %v", root.ID, roots[0])
		}
	})

	t.Run("Find", func(t *testing.T) {
		res, err := cats.Find(leaf.ID)
		if err != nil {
			t.Errorf("failed when finding the leaf: %v", err)
		}
		if res.ID != leaf.ID {
			t.Errorf("found the leaf (id: %v) but got the wrong ID (%v)", leaf.ID, res.ID)
		}
	})

	t.Run("FindAll", func(t *testing.T) {
		list, err := cats.FindAll("WHERE name = $1", "leaf")
		if err != nil {
			t.Errorf("failed when finding by name: %v", err)
		}
		if len(list) != 1 {
			t.Errorf("found more than one hit looking for the leaf")
		}
		if list[0].ID != leaf.ID {
			t.Errorf("found the leaf by name (id: %v) but got the wrong ID (%v)", leaf.ID, list[0].ID)
		}
	})
}

package categories

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"
)

// JournalList returns a list of all journals available on the server. Pass the
// URL for the root of the Sciveyor server (probably https://www.sciveyor.com/).
func JournalList(appURL string) ([]string, error) {
	// Parse the base URL
	url, err := url.Parse(appURL)
	if err != nil {
		return []string{}, err
	}

	// Replace the path with the path to the API endpoint
	url.Path = "/api/v1/lists/journals"
	url.Query().Set("limit", "9999")

	// Make the request
	req, err := http.NewRequest("GET", url.String(), nil)
	if err != nil {
		return []string{}, err
	}

	req.Header.Add("Accept", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return []string{}, err
	}
	defer resp.Body.Close()

	// Read the body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return []string{}, err
	}

	var list []map[string]interface{}
	if err = json.Unmarshal(body, &list); err != nil {
		return []string{}, err
	}

	// Transform the result and return
	ret := []string{}

	for _, hit := range list {
		value, ok := hit["value"].(string)
		if !ok {
			return []string{}, errors.New("value in hit is not a string")
		}

		ret = append(ret, value)
	}

	return ret, nil
}

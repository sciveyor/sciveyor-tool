package categories

import (
	"reflect"

	"github.com/lib/pq"
)

// Category is the representation of a journal category in the database.
type Category struct {
	ID       string
	Name     string
	Journals []string
	Path     []string

	parent *Categories
}

// Reload refreshes the information about this category from the database.
func (c *Category) Reload() error {
	reload, err := c.parent.Find(c.ID)
	if err != nil {
		return err
	}

	c.Name = reload.Name
	c.Journals = reload.Journals
	c.Path = reload.Path
	return nil
}

// Root returns the root to which this category belongs (or itself).
func (c *Category) Root() (Category, error) {
	if len(c.Path) == 0 {
		return *c, nil
	}

	return c.parent.Find(c.Path[0])
}

// Parent returns the parent of this category, or a blank category if this is a root.
func (c *Category) Parent() (Category, error) {
	if len(c.Path) == 0 {
		return Category{}, nil
	}

	return c.parent.Find(c.Path[len(c.Path)-1])
}

// Ancestors returns all ancestors of this category (or []).
func (c *Category) Ancestors() ([]Category, error) {
	if len(c.Path) == 0 {
		return []Category{}, nil
	}
	return c.parent.FindAll("WHERE id = ANY ($1)", pq.Array(c.Path))
}

// Descendants returns all descendants of this category (or []).
func (c *Category) Descendants() ([]Category, error) {
	return c.parent.FindAll("WHERE $1 = ANY (path)", c.ID)
}

// Children returns all direct children of this category.
func (c *Category) Children() ([]Category, error) {
	newPath := append(c.Path, c.ID)
	return c.parent.FindAll("WHERE path = ($1)", pq.Array(newPath))
}

// SetName changes the name of this category.
func (c *Category) SetName(name string) error {
	if c.Name == name {
		return nil
	}
	c.Name = name

	_, err := c.parent.db.Exec("UPDATE categories SET name = $1 WHERE id = $2 RETURNING id",
		c.Name, c.ID)
	return err
}

// SetJournals changes the journals for this category.
func (c *Category) SetJournals(journals []string) error {
	if reflect.DeepEqual(c.Journals, journals) {
		return nil
	}

	c.Journals = make([]string, len(journals))
	copy(c.Journals, journals)

	_, err := c.parent.db.Exec("UPDATE categories SET journals = $1 WHERE id = $2",
		pq.Array(c.Journals), c.ID)
	return err
}

// SetParent moves this category to a new parent.
func (c *Category) SetParent(parent Category) error {
	oldPath := make([]string, len(c.Path))
	copy(oldPath, c.Path)
	oldLen := len(oldPath)

	// See if we were asked to make this a root
	newPath := []string{}
	if parent.ID != "" {
		newPath = append(parent.Path, parent.ID)
	}

	// Do this as an atomic DB transaction so we don't pollute if we fail
	tx, err := c.parent.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	// Walk all of our descendants, replacing oldPath with newPath
	descendants, err := c.Descendants()
	if err != nil {
		return err
	}

	for _, desc := range descendants {
		newDescPath := append(newPath, desc.Path[oldLen:]...)

		_, err = tx.Exec("UPDATE categories SET path = $1 WHERE id = $2",
			pq.Array(newDescPath), desc.ID)
		if err != nil {
			return err
		}
	}

	// Change our own path
	_, err = tx.Exec("UPDATE categories SET path = $1 WHERE id = $2",
		pq.Array(newPath), c.ID)
	if err != nil {
		return err
	}

	c.Path = newPath

	if err = tx.Commit(); err != nil {
		return err
	}

	return nil
}

// GatherJournals will set the journals array for this category to the sum of
// all journals of all our descendants.
//
// This is a recursive operation, which will also perform gather for all of our
// descendants.
func (c *Category) GatherJournals() error {
	children, err := c.Children()
	if err != nil {
		return err
	}

	// Don't touch the journals present in leaf nodes
	if len(children) == 0 {
		return nil
	}

	// Run the gather operation for each of our children, collect results
	journalSet := map[string]bool{}

	for _, child := range children {
		err = child.GatherJournals()
		if err != nil {
			return err
		}

		for _, journal := range child.Journals {
			journalSet[journal] = true
		}
	}

	// Set our journals to the sum of our immediate children
	newJournals := []string{}
	for journal := range journalSet {
		newJournals = append(newJournals, journal)
	}

	return c.SetJournals(newJournals)
}

// Delete deletes this category from the database.
func (c *Category) Delete() error {
	_, err := c.parent.db.Exec("DELETE FROM categories WHERE id = $1", c.ID)
	return err
}

package categories

import (
	"reflect"
	"sort"
	"testing"
)

func TestCategory(t *testing.T) {
	cats := getTestCategories(t)

	// Add a root and two leaves
	root, err := cats.Add("root", []string{"PNTDS", "PLoS Neglected Tropical Diseases"}, []string{})
	if err != nil {
		t.Errorf("failed when adding root category: %v", err)
	}

	leaf, err := cats.Add("leaf", []string{"PNTDS"}, []string{root.ID})
	if err != nil {
		t.Errorf("failed when adding leaf category: %v", err)
	}

	grandleaf, err := cats.Add("grandleaf", []string{}, []string{root.ID, leaf.ID})
	if err != nil {
		t.Errorf("failed when adding grandleaf category: %v", err)
	}

	t.Run("Root", func(t *testing.T) {
		ret, err := grandleaf.Root()
		if err != nil {
			t.Errorf("failed getting root of grandleaf: %v", err)
		}
		if ret.ID != root.ID {
			t.Errorf("expected to see root (%v) for grandleaf, got %v", root.ID, ret.ID)
		}

		ret, err = leaf.Root()
		if err != nil {
			t.Errorf("failed getting root of leaf: %v", err)
		}
		if ret.ID != root.ID {
			t.Errorf("expected to see root (%v) for leaf, got %v", root.ID, ret.ID)
		}

		ret, err = root.Root()
		if err != nil {
			t.Errorf("failed getting root of root: %v", err)
		}
		if ret.ID != root.ID {
			t.Errorf("expected root to return itself (%v) for Root(), got %v", root.ID, ret.ID)
		}
	})

	t.Run("Parent", func(t *testing.T) {
		ret, err := grandleaf.Parent()
		if err != nil {
			t.Errorf("failed getting parent of grandleaf: %v", err)
		}
		if ret.ID != leaf.ID {
			t.Errorf("expected to see leaf (%v) as parent for grandleaf, got %v", leaf.ID, ret.ID)
		}

		ret, err = leaf.Parent()
		if err != nil {
			t.Errorf("failed getting parent of leaf: %v", err)
		}
		if ret.ID != root.ID {
			t.Errorf("expected to see root (%v) as parent for leaf, got %v", root.ID, ret.ID)
		}

		ret, err = root.Parent()
		if err != nil {
			t.Errorf("failed getting parent of root: %v", err)
		}
		if ret.ID != "" {
			t.Errorf("expected to see nil as parent for root, got %v", ret.ID)
		}
	})

	t.Run("Ancestors", func(t *testing.T) {
		ancestors, err := grandleaf.Ancestors()
		if err != nil {
			t.Errorf("failed getting ancestors of grandleaf: %v", err)
		}
		if len(ancestors) != 2 {
			t.Errorf("expected two ancestors for grandleaf, got %v", len(ancestors))
		}

		ancestors, err = leaf.Ancestors()
		if err != nil {
			t.Errorf("failed getting ancestors of leaf: %v", err)
		}
		if len(ancestors) != 1 {
			t.Errorf("expected one ancestor for leaf, got %v", len(ancestors))
		}

		ancestors, err = root.Ancestors()
		if err != nil {
			t.Errorf("failed getting ancestors of root: %v", err)
		}
		if len(ancestors) != 0 {
			t.Errorf("expected no ancestors for root, got %v", len(ancestors))
		}
	})

	t.Run("Descendants", func(t *testing.T) {
		descendants, err := grandleaf.Descendants()
		if err != nil {
			t.Errorf("failed getting descendants of grandleaf: %v", err)
		}
		if len(descendants) != 0 {
			t.Errorf("expected no ancestors for grandleaf, got %v", len(descendants))
		}

		descendants, err = leaf.Descendants()
		if err != nil {
			t.Errorf("failed getting descendants of leaf: %v", err)
		}
		if len(descendants) != 1 {
			t.Errorf("expected one descendant for leaf, got %v", len(descendants))
		}

		descendants, err = root.Descendants()
		if err != nil {
			t.Errorf("failed getting descendants of root: %v", err)
		}
		if len(descendants) != 2 {
			t.Errorf("expected two descendants for leaf, got %v", len(descendants))
		}
	})

	t.Run("Children", func(t *testing.T) {
		children, err := grandleaf.Children()
		if err != nil {
			t.Errorf("failed getting children of grandleaf: %v", err)
		}
		if len(children) != 0 {
			t.Errorf("expected no children for grandleaf, got %v", len(children))
		}

		children, err = leaf.Children()
		if err != nil {
			t.Errorf("failed getting children of leaf: %v", err)
		}
		if len(children) != 1 {
			t.Errorf("expected one child for leaf, got %v", len(children))
		}
		if children[0].ID != grandleaf.ID {
			t.Errorf("expected grandleaf as child of leaf (%v), got %v", grandleaf.ID, children[0].ID)
		}

		children, err = root.Children()
		if err != nil {
			t.Errorf("failed getting children of root: %v", err)
		}
		if len(children) != 1 {
			t.Errorf("expected one child for root, got %v", len(children))
		}
		if children[0].ID != leaf.ID {
			t.Errorf("expected leaf as child of root (%v), got %v", leaf.ID, children[0].ID)
		}
	})

	t.Run("SetName", func(t *testing.T) {
		err := root.SetName("asdf")
		if err != nil {
			t.Errorf("failed setting name of root: %v", err)
		}

		ret, err := cats.FindAll("WHERE name = $1", "asdf")
		if err != nil {
			t.Errorf("failed finding new name for root: %v", err)
		}
		if ret[0].ID != root.ID {
			t.Errorf("did not get root when searching for the new name")
		}

		root.SetName("root")
	})

	t.Run("SetJournals", func(t *testing.T) {
		err := grandleaf.SetJournals([]string{"asdf"})
		if err != nil {
			t.Errorf("failed setting new journals for grandleaf: %v", err)
		}

		ret, err := cats.Find(grandleaf.ID)
		if err != nil {
			t.Errorf("failed reloading grandleaf: %v", err)
		}
		if ret.Journals[0] != "asdf" {
			t.Errorf("did not set new journals for grandleaf (%v)", ret.Journals)
		}

		grandleaf.SetJournals([]string{})
	})

	// SetParent, to an empty category (i.e., make this node a root node)
	t.Run("SetParentRoot", func(t *testing.T) {
		err := leaf.SetParent(Category{})
		if err != nil {
			t.Errorf("failed setting leaf to root: %v", err)
		}

		newRoot, err := leaf.Root()
		if err != nil {
			t.Errorf("failed getting root for leaf: %v", err)
		}
		if newRoot.ID != leaf.ID {
			t.Errorf("leaf isn't root after being set to root")
		}

		// Make sure it's reflected in the DB
		newLeaf, err := cats.Find(leaf.ID)
		if err != nil {
			t.Errorf("failed reloading leaf: %v", err)
		}
		if len(newLeaf.Path) != 0 {
			t.Errorf("leaf root didn't save to database")
		}

		// Make sure it's reflected in the children
		err = grandleaf.Reload()
		if err != nil {
			t.Errorf("failed reloading grandleaf: %v", err)
		}

		newRoot, err = grandleaf.Root()
		if err != nil {
			t.Errorf("failed getting root for grandleaf: %v", err)
		}
		if newRoot.ID != leaf.ID {
			t.Errorf("leaf isn't root for grandleaf after being set to root")
		}

		// Make sure it's reflected in the root
		err = root.Reload()
		if err != nil {
			t.Errorf("failed reloading root: %v", err)
		}

		descendants, err := root.Descendants()
		if err != nil {
			t.Errorf("failed getting descendants for root: %v", err)
		}
		if len(descendants) != 0 {
			t.Errorf("expected root to have no descendants, had %v", len(descendants))
		}

		leaf.SetParent(root)
		root.Reload()
		leaf.Reload()
		grandleaf.Reload()
	})

	t.Run("SetParentNonRoot", func(t *testing.T) {
		err := grandleaf.SetParent(root)
		if err != nil {
			t.Errorf("failed setting parent for grandleaf: %v", err)
		}

		// Leaf should have no children
		children, err := leaf.Children()
		if err != nil {
			t.Errorf("failed getting children for leaf: %v", err)
		}
		if len(children) != 0 {
			t.Errorf("expected leaf to have no children, had %v", len(children))
		}

		// Root should have two children
		children, err = root.Children()
		if err != nil {
			t.Errorf("failed getting children for leaf: %v", err)
		}
		if len(children) != 2 {
			t.Errorf("expected root to have two children, had %v", len(children))
		}

		grandleaf.SetParent(leaf)
		grandleaf.Reload()
	})

	t.Run("GatherJournals", func(t *testing.T) {
		// Set up
		thirdleaf, err := cats.Add("thirdleaf", []string{"One", "Two"}, []string{root.ID, leaf.ID})
		if err != nil {
			t.Errorf("failed when adding thirdleaf category: %v", err)
		}

		if err = grandleaf.SetJournals([]string{"Three"}); err != nil {
			t.Errorf("failed setting journals of grandleaf: %v", err)
		}

		if err = root.GatherJournals(); err != nil {
			t.Errorf("failed gathering journals on root: %v", err)
		}

		// Make sure we left leaves alone
		if len(thirdleaf.Journals) != 2 {
			t.Errorf("GatherJournals changed thirdleaf's journal list (%v)", thirdleaf.Journals)
		}
		if len(grandleaf.Journals) != 1 {
			t.Errorf("GatherJournals changed thirdleaf's journal list (%v)", grandleaf.Journals)
		}

		// Check that we gathered all three into leaf and root
		if err = leaf.Reload(); err != nil {
			t.Errorf("failed reloading leaf: %v", err)
		}

		if len(leaf.Journals) != 3 {
			t.Errorf("GatherJournals didn't gather into leaf (%v)", leaf.Journals)
		}
		if len(root.Journals) != 3 {
			t.Errorf("GatherJournals didn't gather into root (%v)", root.Journals)
		}

		sort.Strings(root.Journals)
		if !reflect.DeepEqual(root.Journals, []string{"One", "Three", "Two"}) {
			t.Errorf("gathered the wrong set of journals (%v)", root.Journals)
		}

		// Clean up
		grandleaf.SetJournals([]string{})
		thirdleaf.Delete()

		root.Reload()
		leaf.Reload()
	})

	t.Run("Delete", func(t *testing.T) {
		newChild, err := cats.Add("new", []string{}, []string{root.ID, leaf.ID})
		if err != nil {
			t.Errorf("failed when adding new category: %v", err)
		}

		children, err := leaf.Children()
		if err != nil {
			t.Errorf("failed getting children for leaf: %v", err)
		}
		if len(children) != 2 {
			t.Errorf("didn't successfully add child to leaf")
		}

		if err = newChild.Delete(); err != nil {
			t.Errorf("failed deleting new child: %v", err)
		}

		children, err = leaf.Children()
		if err != nil {
			t.Errorf("failed refreshing children for leaf: %v", err)
		}
		if len(children) != 1 {
			t.Errorf("didn't successfully delete child from leaf")
		}
	})
}

package categories

import (
	"database/sql"
	"sort"

	// Unnamed import for PostgreSQL driver
	_ "github.com/lib/pq"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

// Config encapsulates the list of parameters needed to start the editor.
type EditorConfig struct {
	AppAddress string
	DbAddress  string

	app   *tview.Application
	pages *tview.Pages
	cats  *Categories
}

var editor *EditorConfig

const (
	ADD_BUTTON  = "Add a category here…"
	CHECKED_BOX = " [✓]"
	EMPTY_BOX   = " [ ]"
)

func messageBox(text string, fatal bool) {
	modal := tview.NewModal().
		SetText(text)

	if fatal {
		modal.AddButtons([]string{"Quit"}).
			SetDoneFunc(func(int, string) { editor.app.Stop() })
	} else {
		modal.AddButtons([]string{"OK"}).
			SetDoneFunc(func(int, string) {
				editor.pages.RemovePage("modal")
			})
	}

	editor.pages.AddPage("modal", modal, false, true)
}

// journalBox shows a modal form for choosing a list of journals. The done
// callback will be called if and when the user presses <TAB> to confirm the
// list of journals.
func journalBox(current []string, done func(journals []string)) {
	fullList, err := JournalList(editor.AppAddress)
	if err != nil {
		return
	}

	sort.Strings(current)
	sort.Strings(fullList)

	table := tview.NewTable().
		SetBorders(false).
		SetSelectable(true, false)

	// Add all the cells
	for idx, journal := range fullList {
		cell := tview.NewTableCell("").
			SetExpansion(0)

		// Is this journal checked already?
		i := sort.SearchStrings(current, journal)
		if i < len(current) && current[i] == journal {
			cell.SetTextColor(tcell.ColorGreen).
				SetText(CHECKED_BOX)
		} else {
			cell.SetTextColor(tcell.ColorRed).
				SetText(EMPTY_BOX)
		}

		table.SetCell(idx, 0, cell)

		cell = tview.NewTableCell(journal).
			SetExpansion(1)
		table.SetCell(idx, 1, cell)
	}

	// Toggle selection when we press enter on a row
	table.SetSelectedFunc(func(row, _ int) {
		checkCell := table.GetCell(row, 0)

		if checkCell.Color == tcell.ColorGreen {
			checkCell.SetTextColor(tcell.ColorRed).
				SetText(EMPTY_BOX)
		} else {
			checkCell.SetTextColor(tcell.ColorGreen).
				SetText(CHECKED_BOX)
		}
	})

	// Call the callback if the user approves
	table.SetDoneFunc(func(key tcell.Key) {
		if key != tcell.KeyEscape {
			// User confirms, gather the list and call the callback
			journals := []string{}

			for i := 0; i < table.GetRowCount(); i++ {
				checkCell := table.GetCell(i, 0)
				if checkCell.Color == tcell.ColorGreen {
					journalCell := table.GetCell(i, 1)
					journals = append(journals, journalCell.Text)
				}
			}

			done(journals)
		}

		// Close this page in any case
		editor.pages.RemovePage("journals")
	})

	// Put the table in a frame with instructions
	frame := tview.NewFrame(table).
		AddText("press <TAB> to save, or <Escape> to cancel", false, tview.AlignCenter, tcell.ColorReset)

	frame.SetBorder(true)
	frame.SetBorderColor(tcell.ColorOlive)
	frame.SetTitle(" Choose the journals to include: ")
	frame.SetTitleColor(tcell.ColorOlive)

	// Put the frame in a grid
	grid := tview.NewGrid().
		SetColumns(0, -10, 0).
		SetRows(0, -10, 0).
		AddItem(frame, 1, 1, 1, 1, 0, 0, true)

	editor.pages.AddPage("journals", grid, true, true)
}

// categoryBox shows a modal form for editing or creating a category. Initial
// values for the form will be populated from source. The callback will be
// called if the user closes the box by pressing OK.
func categoryBox(title string, source Category, done func(ret Category)) {
	// Here's where we'll store the category we're building
	ret := source

	// Create the form
	form := tview.NewForm()

	// Name
	nameField := tview.NewInputField().
		SetLabel("Name:").
		SetChangedFunc(func(text string) {
			ret.Name = text
		})

	form.AddFormItem(nameField)

	// Journals
	journalField := tview.NewInputField().
		SetLabel("Journals:").
		SetPlaceholder("(press <enter> to edit)").
		SetAcceptanceFunc(func(string, rune) bool {
			// make this field read-only by rejecting all changes
			return false
		})

	// Launch the journal editor when we press enter on this field
	journalField.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		key := event.Key()
		if key == tcell.KeyEnter {
			journalBox(ret.Journals, func(result []string) {
				ret.Journals = make([]string, len(result))
				copy(ret.Journals, result)
			})

			return nil
		}

		return event
	})

	form.AddFormItem(journalField)

	// Buttons
	form.AddButton("OK", func() {
		done(ret)
		editor.pages.RemovePage("category")
	})
	form.AddButton("Cancel", func() {
		editor.pages.RemovePage("category")
	})

	// Set box properties
	form.SetBorder(true).
		SetBorderColor(tcell.ColorOlive).
		SetTitle(" " + title + " ").
		SetTitleColor(tcell.ColorOlive)

	// Put the form in a grid
	grid := tview.NewGrid().
		SetColumns(0, -5, 0).
		SetRows(0, 9, 0).
		AddItem(form, 1, 1, 1, 1, 0, 0, true)

	editor.pages.AddPage("category", grid, true, true)
}

func addCategory(cat Category) {
	parent := cat.Name
	var path []string

	if len(parent) == 0 {
		parent = "at the root"
		path = []string{}
	} else {
		parent = "as child of " + parent
		path = append(cat.Path, cat.ID)
	}

	categoryBox(
		"Adding a new category "+parent,
		Category{},
		func(ret Category) {
			_, err := editor.cats.Add(ret.Name, ret.Journals, path)
			if err != nil {
				messageBox("Failed to add category! "+err.Error(), true)
			}
		})
}

func deleteCategory(cat Category) {
	messageBox("delete the thing", false)
}

func editCategory(cat Category) {
	messageBox("edit the thing", false)
}

func showCategory(cat Category) {
	messageBox("first show box", false)
}

func onEnter(node *tview.TreeNode) {
	ref := node.GetReference()
	if ref == nil {
		// Spacer or something, ignore
		return
	}

	cat, ok := ref.(Category)
	if !ok {
		return
	}

	// If we're on the add button, pop up the add box here
	if node.GetText() == ADD_BUTTON {
		addCategory(cat)
		return
	}

	// Show the regular display box
	showCategory(cat)
}

func addToTree(node *tview.TreeNode, parent Category, list []Category) error {
	for _, cat := range list {
		// Add this category
		child := tview.NewTreeNode(cat.Name).
			SetReference(cat).
			SetSelectable(true)
		node.AddChild(child)

		grandchildren, err := cat.Children()
		if err != nil {
			return err
		}

		addToTree(child, cat, grandchildren)
	}

	// Ater all the categories at this level, add the "add-category" button for
	// this level
	addHere := tview.NewTreeNode(ADD_BUTTON).
		SetReference(parent).
		SetSelectable(true).
		SetColor(tcell.ColorOlive)
	node.AddChild(addHere)

	return nil
}

func buildTree() (*tview.TreeNode, error) {
	root := tview.NewTreeNode("[Journal Categories]").
		SetColor(tcell.ColorOlive).
		SetSelectable(false)

	roots, err := editor.cats.Roots()
	if err != nil {
		return nil, err
	}
	if err = addToTree(root, Category{}, roots); err != nil {
		return nil, err
	}

	return root, nil
}

func Editor(config ...EditorConfig) error {
	editor = new(EditorConfig)
	if len(config) >= 0 {
		editor.AppAddress = config[0].AppAddress
		editor.DbAddress = config[0].DbAddress
	}

	// Connect to the database and load the categories
	db, err := sql.Open("postgres", editor.DbAddress)
	if err != nil {
		return err
	}

	editor.cats = &Categories{db: db}

	// Build the initial tree view
	root, err := buildTree()
	if err != nil {
		return err
	}

	tree := tview.NewTreeView().
		SetRoot(root).
		SetCurrentNode(root).
		SetSelectedFunc(onEnter)

	// Put the tree in a frame with instructions
	frame := tview.NewFrame(tree).
		AddText("select a category and press <enter> to edit; press <C-q> to exit", false, tview.AlignCenter, tcell.ColorReset)

	// Make a multi-stack pages for our dialog boxes
	editor.pages = tview.NewPages().
		AddPage("root", frame, true, true)

	// Add the pages to the app, register q as the quit key
	editor.app = tview.NewApplication().
		SetRoot(editor.pages, true).
		SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
			if event.Key() == tcell.KeyCtrlQ || event.Key() == tcell.KeyCtrlX {
				editor.app.Stop()
				return nil
			}

			return event
		})

	if err := editor.app.Run(); err != nil {
		return err
	}
	return nil
}

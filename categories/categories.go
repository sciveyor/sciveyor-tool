package categories

import (
	"database/sql"

	"github.com/lib/pq"
)

// Categories is the representation of an entire set of categories in the
// database.
type Categories struct {
	db *sql.DB
}

// Find returns the category with the given ID.
func (c *Categories) Find(id string) (Category, error) {
	var ret Category
	err := c.db.QueryRow("SELECT id, name, journals, path FROM categories WHERE id=$1",
		id).Scan(&ret.ID, &ret.Name, pq.Array(&ret.Journals), pq.Array(&ret.Path))
	if err != nil {
		return Category{}, err
	}

	ret.parent = c
	return ret, nil
}

// FindAll returns all categories that match the given query and bindings.
func (c *Categories) FindAll(query string, args ...interface{}) ([]Category, error) {
	// Run the query
	rows, err := c.db.Query("SELECT id, name, journals, path FROM categories "+
		query+" ORDER BY name, id", args...)
	if err != nil {
		return []Category{}, err
	}
	defer rows.Close()

	// Get all the rows
	ret := []Category{}

	for rows.Next() {
		var cat Category

		if err := rows.Scan(&cat.ID, &cat.Name, pq.Array(&cat.Journals), pq.Array(&cat.Path)); err != nil {
			return []Category{}, err
		}

		cat.parent = c
		ret = append(ret, cat)
	}
	if err = rows.Err(); err != nil {
		return ret, err
	}
	return ret, nil
}

// Add adds the given category to the database.
func (c *Categories) Add(name string, journals, path []string) (Category, error) {
	// Do the database insert
	var newID string

	err := c.db.QueryRow("INSERT INTO categories(name, journals, path) VALUES ($1, $2, $3) RETURNING id",
		name, pq.Array(journals), pq.Array(path)).Scan(&newID)
	if err != nil {
		return Category{}, err
	}

	return Category{
		ID:       newID,
		Name:     name,
		Journals: journals,
		Path:     path,
		parent:   c,
	}, nil
}

// Roots returns all of the root categories.
func (c *Categories) Roots() ([]Category, error) {
	return c.FindAll("WHERE path='{}'")
}

package categories

import (
	"testing"
)

func TestJournalList(t *testing.T) {
	testServerUrl := "http://localhost:3000"

	list, err := JournalList(testServerUrl)
	if err != nil {
		t.Errorf("expected to get journal list, got %v", err)
	}

	if len(list) != 2 {
		t.Errorf("expected to get journal list of size 2, got %v", len(list))
	}
	if list[0] != "PLoS Neglected Tropical Diseases" {
		t.Errorf("expected 'PLoS Neglected Tropical Diseases', got %v", list[0])
	}
}

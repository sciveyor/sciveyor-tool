package util

import (
	"context"
	"testing"
)

const minimal = `{
	"schema": "https://data.sciveyor.com/schema",
	"version": 5,
	"id": "thisisanid",
	"license": "some terms",
	"licenseUrl": "https://some.url.com/",
	"dataSource": "some place",
	"dataSourceUrl": "https://data.sciveyor.com/dataplace",
	"dataSourceVersion": 1,
	"type": "article"
}`

const minimalExtra = `{
	"schema": "https://data.sciveyor.com/schema",
	"version": 5,
	"id": "thisisanid",
	"license": "some terms",
	"licenseUrl": "https://some.url.com/",
	"dataSource": "some place",
	"dataSourceUrl": "https://data.sciveyor.com/dataplace",
	"dataSourceVersion": 1,
	"type": "article",
	"anextrafield": "this is not in the schema"
}`

const minimalDate = `{
	"schema": "https://data.sciveyor.com/schema",
	"version": 5,
	"id": "thisisanid",
	"license": "some terms",
	"licenseUrl": "https://some.url.com/",
	"dataSource": "some place",
	"dataSourceUrl": "https://data.sciveyor.com/dataplace",
	"dataSourceVersion": 1,
	"type": "article",
	"date": "2009-01-04"
}`

const badDate = `{
	"schema": "https://data.sciveyor.com/schema",
	"version": 5,
	"id": "thisisanid",
	"license": "some terms",
	"licenseUrl": "https://some.url.com/",
	"dataSource": "some place",
	"dataSourceUrl": "https://data.sciveyor.com/dataplace",
	"dataSourceVersion": 1,
	"type": "article",
	"date": "2009-1-4"
}`

func TestLoadSchema(t *testing.T) {
	schema, err := LoadSchema(true)
	if err != nil {
		t.Errorf("should load schema without error, got %v", err)
	}

	schemaErrs, err := schema.ValidateBytes(context.Background(), []byte("{}"))
	if err != nil {
		t.Errorf("should validate empty document, got %v", err)
	}
	if len(schemaErrs) == 0 {
		t.Error("expected to fail validating an empty document, did not")
	}

	schemaErrs, err = schema.ValidateBytes(context.Background(), []byte(minimal))
	if err != nil {
		t.Errorf("should validate minimal document, got %v", err)
	}
	if len(schemaErrs) != 0 {
		t.Errorf("expected to succeed validating an empty document, got %v",
			schemaErrs)
	}

	schemaErrs, err = schema.ValidateBytes(context.Background(),
		[]byte(minimalExtra))
	if err != nil {
		t.Errorf("should validate extra document, got %v", err)
	}
	if len(schemaErrs) == 0 {
		t.Errorf("expected to fail validating extra attributes in strict mode, did not")
	}
}

func TestLoadSchemaNonStrict(t *testing.T) {
	schema, err := LoadSchema(false)
	if err != nil {
		t.Errorf("should load schema without error, got %v", err)
	}

	schemaErrs, err := schema.ValidateBytes(context.Background(), []byte("{}"))
	if err != nil {
		t.Errorf("should validate empty document, got %v", err)
	}
	if len(schemaErrs) == 0 {
		t.Error("expected to fail validating an empty document, did not")
	}

	schemaErrs, err = schema.ValidateBytes(context.Background(), []byte(minimal))
	if err != nil {
		t.Errorf("should validate minimal document, got %v", err)
	}
	if len(schemaErrs) != 0 {
		t.Errorf("expected to succeed validating an empty document, got %v",
			schemaErrs)
	}

	schemaErrs, err = schema.ValidateBytes(context.Background(),
		[]byte(minimalExtra))
	if err != nil {
		t.Errorf("should validate extra document, got %v", err)
	}
	if len(schemaErrs) != 0 {
		t.Errorf("expected to succeed validating extra attributes in non-strict mode, got %v", schemaErrs)
	}
}

func TestSchemaValidateDate(t *testing.T) {
	schema, err := LoadSchema(false)
	if err != nil {
		t.Errorf("should load schema without error, got %v", err)
	}

	schemaErrs, err := schema.ValidateBytes(context.Background(),
		[]byte(minimalDate))
	if err != nil {
		t.Errorf("should validate document with short date, got %v", err)
	}
	if len(schemaErrs) != 0 {
		t.Errorf("expected to succeed validating a document with a short date, got %v",
			schemaErrs)
	}

	schemaErrs, err = schema.ValidateBytes(context.Background(),
		[]byte(badDate))
	if err != nil {
		t.Errorf("should validate bad-date document, got %v", err)
	}
	if len(schemaErrs) == 0 {
		t.Errorf("expected to fail validating document with bad date, did not")
	}
}

func TestLoadPartialSchema(t *testing.T) {
	ps, err := LoadPartialSchema()
	if err != nil {
		t.Errorf("should load partial schema without error, got %v", err)
	}

	// Check a few representative properties
	_, ok := ps.Properties["id"]
	if !ok {
		t.Errorf("schema properties is missing 'id'")
	}
	_, ok = ps.Properties["type"]
	if !ok {
		t.Errorf("schema properties is missing 'type'")
	}

	_, ok = ps.AuthorProperties["name"]
	if !ok {
		t.Errorf("schema authorProperties is missing 'name'")
	}
	_, ok = ps.AuthorProperties["last"]
	if !ok {
		t.Errorf("schema authorProperties is missing 'last'")
	}
}

package util

import (
	"bytes"
	"encoding/json"
	"errors"

	// embed must be imported blank in order to function
	_ "embed"

	"github.com/qri-io/jsonschema"
)

// JSONSchema contains the bytes from the schema JSON file, embedded as a Go
// constant.
//go:embed schema/schema.json
var JSONSchema []byte

// PartialSchema represents a few of the lists of properties found in the
// schema that we need to pass data to Solr.
//
// The only purpose for which we load the schema is to query the lists of
// acceptable properties that may appear in a document or in an author object.
// We therefore just extract the "properties" keys from both of the object
// definitions and store them here.
type PartialSchema struct {
	Properties       map[string]interface{}
	AuthorProperties map[string]interface{}
}

// LoadSchema reads in the schema and parses it to a JSON Schema object.
//
// If the `strict` parameter is true, then the schema object that results will
// throw errors whenever a parameter is present in a document that is not found
// in the schema (extra parameters will generate errors). Pass false to disable
// this behavior (and permit extra parameters).
func LoadSchema(strict bool) (*jsonschema.Schema, error) {
	// Copy the static schema bytes
	schemaData := make([]byte, len(JSONSchema))
	copy(schemaData, JSONSchema)

	// If we're not operating in strict mode, we need to swap the two
	// "additionalProperties" values
	if !strict {
		find := []byte("\"additionalProperties\": false")
		replace := []byte("\"additionalProperties\": true")
		schemaData = bytes.ReplaceAll(schemaData, find, replace)
	}

	// Workaround for qri-io/jsonschema#92 - the date-time parsing is incorrect,
	// and will give false negatives; fix it by replacing with our own regex for
	// the time being
	find := []byte("\"format\": \"date-time\"")
	replace := []byte("\"pattern\": \"^([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z[0-9]{2})|([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2})|([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2})|([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2})|([0-9]{4}-[0-9]{2}-[0-9]{2})|([0-9]{4}-[0-9]{2})|([0-9]{4})$\"")
	schemaData = bytes.ReplaceAll(schemaData, find, replace)

	// Parse it
	rs := &jsonschema.Schema{}
	if err := json.Unmarshal(schemaData, rs); err != nil {
		return nil, err
	}

	return rs, nil
}

// LoadPartialSchema reads in the schema and parses it to a PartialSchema.
//
// Note that since we are not doing full schema validation, this is by no means
// a complete parsing of the schema. If you want that, see LoadSchema. This
// function only extracts the few fields that we require to "clean" Mongo
// documents before passing them to Solr. (See sanitizeMongoDocument for more
// information.)
func LoadPartialSchema() (*PartialSchema, error) {
	// Copy the static schema bytes
	schemaData := make([]byte, len(JSONSchema))
	copy(schemaData, JSONSchema)

	// Parse it
	var schema map[string]interface{}
	if err := json.Unmarshal(schemaData, &schema); err != nil {
		return nil, err
	}

	ret := new(PartialSchema)

	// Get the overall document "properties" key
	propsRaw, ok := schema["properties"]
	if !ok {
		return nil, errors.New("Schema is missing the properties key")
	}
	ret.Properties, ok = propsRaw.(map[string]interface{})
	if !ok {
		return nil, errors.New("Schema properties value is not an object")
	}

	// Get the author-object "properties" key
	authorsRaw, ok := ret.Properties["authors"]
	if !ok {
		return nil, errors.New("Schema properties is missing the authors key")
	}
	authors, ok := authorsRaw.(map[string]interface{})
	if !ok {
		return nil, errors.New("Schema author properties is not an object")
	}

	authorsItemsRaw, ok := authors["items"]
	if !ok {
		return nil, errors.New("Schema author properties is missing the items key")
	}
	authorsItems, ok := authorsItemsRaw.(map[string]interface{})
	if !ok {
		return nil, errors.New("Schema author items is not an object")
	}

	authorsItemsPropsRaw, ok := authorsItems["properties"]
	if !ok {
		return nil, errors.New("Schema author items is missing the properties key")
	}
	ret.AuthorProperties, ok = authorsItemsPropsRaw.(map[string]interface{})
	if !ok {
		return nil, errors.New("Schema author item properties is not an object")
	}

	return ret, nil
}

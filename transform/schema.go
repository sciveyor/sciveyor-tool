package transform

import (
	"codeberg.org/sciveyor/sciveyor-tool/util"
)

// DeleteExtraProperties will delete any properties that are not present
// within the given PartialSchema.
//
// It returns an error, as well as the number of properties that were deleted
// from the document.
func DeleteExtraProperties(doc map[string]interface{},
	ps *util.PartialSchema) (int, error) {
	ret := 0

	// Iterate over the authors and remove anything not in ps.AuthorProperties
	err := iterateDocAuthors(doc, func(author map[string]interface{}) error {
		for key := range author {
			keysToDelete := []string{}

			_, found := ps.AuthorProperties[key]
			if !found {
				// Element not found in the author schema; remove
				keysToDelete = append(keysToDelete, key)
			}

			if len(keysToDelete) > 0 {
				for _, key := range keysToDelete {
					delete(author, key)
					ret++
				}
			}
		}

		return nil
	})

	if err != nil {
		return 0, err
	}

	// Iterate over the rest of the document and remove anything not in
	// ps.Properties
	keysToDelete := []string{}
	for key := range doc {
		_, found := ps.Properties[key]
		if !found {
			// Element not found in the document schema; remove
			keysToDelete = append(keysToDelete, key)
		}
	}

	for _, key := range keysToDelete {
		delete(doc, key)
		ret++
	}

	return ret, nil
}

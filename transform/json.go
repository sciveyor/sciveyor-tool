package transform

import "errors"

// jsonFloatToInt converts numbers (which are JSON-serialized to floats) to
// integers.
//
// Note that for the moment this converter assumes that all these integer fields
// are required!
func jsonFloatToInt(doc map[string]interface{}) error {
	var intFields = []string{
		"version",
		"dataSourceVersion",
	}

	for _, key := range intFields {
		raw, ok := doc[key]
		if !ok {
			return errors.New("Document is missing field " + key)
		}

		fp, ok := raw.(float64)
		if !ok {
			return errors.New("Document field " + key + " is not a float64")
		}

		doc[key] = int(fp)
	}

	return nil
}

// JSONToMongo runs the transforms that should be necessary to take a document
// from a JSON file to MongoDB.
func JSONToMongo(doc map[string]interface{}) error {
	err := jsonFloatToInt(doc)
	if err != nil {
		return nil
	}

	err = StringToGoDate(doc)
	if err != nil {
		return err
	}

	return nil
}

package transform

import (
	"codeberg.org/sciveyor/sciveyor-tool/util"

	"github.com/google/uuid"
)

// addSolrPropertiesToAuthors adds an 'id' and a 'type' to each author object
// so that they will function as child documents.
func addSolrPropertiesToAuthors(doc map[string]interface{}) error {
	return iterateDocAuthors(doc, func(author map[string]interface{}) error {
		// Add a UUID ad a type to the object
		author["id"] = uuid.New().String()
		author["type"] = "author"

		return nil
	})
}

// MongoToSolr runs the transforms that should be necessary to take a document
// retrieved from MongoDB to Solr. Returns the number of fields that were
// removed from the document.
func MongoToSolr(doc map[string]interface{},
	ps *util.PartialSchema) (int, error) {
	// Start by cleaning the document
	ret, err := DeleteExtraProperties(doc, ps)

	// Add extra Solr-specific properties to authors documents
	err = addSolrPropertiesToAuthors(doc)
	if err != nil {
		return 0, err
	}

	// Convert any MongoDB dates to Solr-compatible strings
	err = MongoDateToString(doc)
	if err != nil {
		return 0, err
	}

	return ret, nil
}

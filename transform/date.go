package transform

import (
	"errors"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// rfc3339Parts returns a list of partial ISO 8601/RFC 3339 date formats to
// parse. You should take the first one that succeeds.
//
// Go's date/time parsing doesn't allow for optional arguments, while ISO 8601
// allows you to arbitrarily drop any date or time components as long as you
// don't mess up the formatting of what came before.
func rfc3339Parts() []string {
	return []string{
		"2006-01-02T15:04:05Z07:00",
		"2006-01-02T15:04:05",
		"2006-01-02T15:04",
		"2006-01-02T15",
		"2006-01-02",
		"2006-01",
		"2006",
	}
}

// getDateFields returns a list of date fields. This is a mild hack to get
// around the fact that we can't have constant slices in Go.
func getDateFields() []string {
	return []string{
		"date",
		"dateElectronic",
		"dateAccepted",
		"dateReceived",
	}
}

// MongoDateToString converts the date values in the document from MongoDB
// dates (Unix timestamp) to timezone-free ISO8601 dates in UTC. This is the
// format of date expected by Solr, and is still standards-compliant.
func MongoDateToString(doc map[string]interface{}) error {
	for _, key := range getDateFields() {
		val, found := doc[key]
		if !found {
			continue
		}

		mongoTime, ok := val.(primitive.DateTime)
		if !ok {
			return errors.New("Document date field " + key + " is not a DateTime")
		}

		t := mongoTime.Time().UTC()
		doc[key] = t.Format("2006-01-02T15:04:05Z")
	}

	return nil
}

// StringToGoDate converts the date values in the document from RFC3339
// strings to Go time.Time objects. These will be serialized correctly as
// MongoDB datetime primitives when they are sent across to MongoDB.
func StringToGoDate(doc map[string]interface{}) error {
	for _, key := range getDateFields() {
		val, found := doc[key]
		if !found {
			continue
		}

		rfcDate, ok := val.(string)
		if !ok {
			return errors.New("Document date field " + key + " is not a string")
		}

		success := false

		for _, fmt := range rfc3339Parts() {
			t, err := time.Parse(fmt, rfcDate)
			if err == nil {
				doc[key] = t
				success = true
				break
			}
		}

		if success == false {
			return errors.New("Could not parse " + key + " field, containing " +
				rfcDate)
		}
	}

	return nil
}

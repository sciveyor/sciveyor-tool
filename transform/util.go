package transform

import (
	"errors"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// iterateDocAuthors will call the given callback for each author object in the
// document.
func iterateDocAuthors(doc map[string]interface{},
	cb func(au map[string]interface{}) error) error {
	for key, val := range doc {
		if key != "authors" {
			continue
		}

		// authors should be a BSON array
		authors, ok := val.(primitive.A)
		if !ok {
			return errors.New("Document authors is not an array")
		}

		for _, authorRaw := range authors {
			// Each author should be a dictionary
			author, ok := authorRaw.(map[string]interface{})
			if !ok {
				return errors.New("Document author is not an object")
			}

			err := cb(author)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

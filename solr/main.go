package solr

import (
	"fmt"

	"github.com/vanng822/go-solr/solr"
)

// Config encapsulates the list of parameters needed to connect to Solr.
type Config struct {
	Address    string
	Collection string
}

// Connect connects to a Solr server using the given configuration parameters.
func Connect(cfg *Config) (*solr.SolrInterface, error) {
	// For the moment, this is all we need to do here
	return solr.NewSolrInterface(cfg.Address, cfg.Collection)
}

// Count returns the number of documents in the Solr collection.
func Count(cfg *Config) (int, error) {
	// Connect to Solr
	si, err := Connect(cfg)
	if err != nil {
		return 0, err
	}

	// Build a Solr query to get everything
	q := solr.NewQuery()
	q.Q("*:*")
	q.DefType("lucene")
	q.Start(0)
	q.Rows(0)

	// Search it
	s := si.Search(q)
	r, err := s.Result(nil)
	if err != nil {
		return 0, err
	}

	return r.Results.NumFound, nil
}

// Commit commits all current changes to Solr.
func Commit(cfg *Config) error {
	// Connect to Solr
	si, err := Connect(cfg)
	if err != nil {
		return err
	}

	resp, err := si.Commit()
	if err != nil {
		return err
	}

	if !resp.Success {
		return fmt.Errorf("failed to send Solr commit, received %v", resp.Result)
	}

	return nil
}

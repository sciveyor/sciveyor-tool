package solr

import (
	"errors"
	"fmt"
	"net/url"
)

// CreateDocument adds the given document to the Solr database.
//
// We simply assume that the given document object has all and only the correct
// properties set that you want to send to Solr; no checking of this object is
// done before it is serialized and sent.
//
// Also note that this function will delete and re-add the given document. It's
// not worth our time to see which fields of the document may have changed and
// compose a proper Solr delta-update message.
func CreateDocument(cfg *Config, document map[string]interface{}) error {
	// Connect to Solr
	si, err := Connect(cfg)
	if err != nil {
		return err
	}

	// Get the ID
	val, ok := document["id"]
	if !ok {
		return errors.New("Document is missing id")
	}
	id, ok := val.(string)
	if !ok {
		return errors.New("Document id value is not a string")
	}

	// Send the delete message, but ignore if it fails (the document might
	// not exist yet)
	//
	// FIXME: Will this auto-delete any orphaned author documents?
	deleteID := map[string]interface{}{"id": id}
	deleteParams := url.Values{}
	deleteParams.Set("commit", "true")
	si.Delete(deleteID, &deleteParams)

	// Send the add message
	message := []map[string]interface{}{document}
	resp, err := si.Update(message, nil)
	if err != nil {
		return nil
	}

	if !resp.Success {
		return fmt.Errorf("failed to send Solr add, received %v", resp.Result)
	}

	return err
}

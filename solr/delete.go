package solr

import (
	"fmt"
)

// DeleteDocument removes the given document from the Solr database.
//
// FIXME: Does this error if the doc doesn't exist?
func DeleteDocument(cfg *Config, id string) error {
	// Connect to Solr
	si, err := Connect(cfg)
	if err != nil {
		return err
	}

	// Send the delete message to Solr
	del := map[string]interface{}{"id": id}
	resp, err := si.Delete(del, nil)
	if err != nil {
		return err
	}

	if !resp.Success {
		return fmt.Errorf("failed to send Solr delete, received %v", resp.Result)
	}

	return nil
}

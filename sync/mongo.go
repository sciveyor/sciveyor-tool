package sync

import (
	"errors"
	"fmt"

	"codeberg.org/sciveyor/sciveyor-tool/mongo"

	"github.com/schollz/progressbar/v3"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// getMongoDocuments returns the full set of documents on the MongoDB server,
// expressed as document objects.
func getMongoDocuments(mc *mongo.Config, sync *Config) ([]document, error) {
	// Count the number of documents, if we are in interactive mode
	var progress *progressbar.ProgressBar
	if sync.Interactive {
		numDocs, err := mongo.Count(mc, nil)
		if err != nil {
			// Can't get a count, so set the count to -1, which will make a progress
			// bar with just a spinner and no end
			numDocs = -1
		}

		fmt.Println("...fetching MongoDB collection")
		progress = progressbar.Default(numDocs)
	}

	ret := []document{}

	projection := bson.D{
		bson.E{Key: "id", Value: 1},
		bson.E{Key: "version", Value: 1},
		bson.E{Key: "dataSourceVersion", Value: 1},
	}

	err := mongo.GetAllDocuments(mc, bson.D{},
		options.Find().SetProjection(projection),
		func(doc bson.M) error {
			// Bump the progress bar
			if sync.Interactive {
				progress.Add(1)
			}

			// Extract the three values we need
			val, ok := doc["id"]
			if !ok {
				return errors.New("Document is missing an id")
			}
			id, ok := val.(string)
			if !ok {
				return errors.New("Document id is not a string")
			}

			val, ok = doc["version"]
			if !ok {
				return errors.New("Document is missing a version")
			}
			version, ok := val.(int32)
			if !ok {
				return errors.New("Document version is not an int")
			}

			val, ok = doc["dataSourceVersion"]
			if !ok {
				return errors.New("Document is missing a dataSourceVersion")
			}
			dataSourceVersion, ok := val.(int32)
			if !ok {
				return errors.New("Document dataSourceVersion is not an int")
			}

			// Add a document for this document
			ret = append(ret,
				document{id, int(version), int(dataSourceVersion)})

			return nil
		})
	if err != nil {
		return nil, err
	}

	return ret, nil
}

package sync

import (
	"fmt"
	"sort"

	"codeberg.org/sciveyor/sciveyor-tool/mongo"
	"codeberg.org/sciveyor/sciveyor-tool/solr"
	"codeberg.org/sciveyor/sciveyor-tool/transform"
	"codeberg.org/sciveyor/sciveyor-tool/util"

	"github.com/schollz/progressbar/v3"
)

// Config specifies all optional parameters and state for Run.
type Config struct {
	BatchSize   int  // number of changes to batch to Solr
	Force       bool // if true, always overwrite Solr with Mongo content
	Interactive bool // if true, show progress bar
	Verbose     bool // if true, print debugging information

	// Private parameters that we use as we go
	mongoDocs []document
	numMongo  int
	solrDocs  []document
	numSolr   int

	numOps int
}

// document represents a thin view of a document object.
//
// We only will synchronize a document from Mongo to Solr if there's been
// an addition or deletion (i.e., if there is a mismatch in the list of ids),
// or if one of the two version fields has been updated. We therefore only
// pull out those three fields from the Mongo database, unless a sync is
// required.
type document struct {
	id                string
	version           int
	dataSourceVersion int
}

// getDocuments will fetch and sort the content of both servers.
func getDocuments(mc *mongo.Config, sc *solr.Config, sync *Config) error {
	// Get the contents of both servers
	docs, err := getMongoDocuments(mc, sync)
	if err != nil {
		return err
	}

	sync.mongoDocs = docs
	sync.numMongo = len(docs)

	docs, err = getSolrDocuments(sc, sync)
	if err != nil {
		return err
	}

	sync.solrDocs = docs
	sync.numSolr = len(docs)

	// Sort them both by id, so that we can use binary search below
	sort.Slice(sync.mongoDocs, func(i, j int) bool {
		return sync.mongoDocs[i].id < sync.mongoDocs[j].id
	})
	sort.Slice(sync.solrDocs, func(i, j int) bool {
		return sync.solrDocs[i].id < sync.solrDocs[j].id
	})

	return nil
}

// createDocuments will create any documents on the Solr server that are
// either missing or have an outdated version number.
//
// This function returns the number of operations completed and any possible
// error.
func createDocuments(mc *mongo.Config, sc *solr.Config, sync *Config) error {
	// Load the partial schema
	ps, err := util.LoadPartialSchema()
	if err != nil {
		return err
	}

	// Start progress bar, if in interactive mode
	var progress *progressbar.ProgressBar
	if sync.Interactive {
		progress = progressbar.Default(int64(sync.numMongo))
		fmt.Println("...looking for documents to create")
	}

	for i := range sync.mongoDocs {
		// Bump the progress bar
		if sync.Interactive {
			progress.Add(1)
		}

		md := &sync.mongoDocs[i]

		idx := sort.Search(sync.numSolr, func(n int) bool {
			return sync.solrDocs[n].id >= md.id
		})

		if idx == sync.numSolr || sync.solrDocs[idx].id != md.id {
			// Get the whole document from MongoDB
			doc, err := mongo.GetDocument(mc, ps, md.id)
			if err != nil {
				return err
			}

			// Sanitize the result
			_, err = transform.MongoToSolr(doc, ps)
			if err != nil {
				return err
			}

			// Found in Mongo, not found in Solr; add to Solr
			err = solr.CreateDocument(sc, doc)
			if err != nil {
				return err
			}

			if sync.Verbose {
				fmt.Printf("   ...created document: %s\n", md.id)
			}

			sync.numOps++
		} else {
			sd := &sync.solrDocs[idx]

			// Found in Mongo and in Solr, check version numbers (unless we're
			// forcing, then just do it anyway)
			if sync.Force ||
				md.version != sd.version ||
				md.dataSourceVersion != sd.dataSourceVersion {
				// Get the whole document from MongoDB
				doc, err := mongo.GetDocument(mc, ps, md.id)
				if err != nil {
					return err
				}

				// Sanitize the result
				_, err = transform.MongoToSolr(doc, ps)
				if err != nil {
					return err
				}

				// Update the document in Solr (id is the Solr uniqueKey, so this will
				// automatically overwrite the previous document)
				err = solr.CreateDocument(sc, doc)
				if err != nil {
					return err
				}

				if sync.Verbose {
					if sync.Force {
						fmt.Printf("   ...forced sync enabled, updated document: %s\n", md.id)
					} else {
						fmt.Printf("   ...version number mismatch, updated document: %s\n", md.id)
					}
				}

				sync.numOps++
			}
		}

		// Commit every few documents to let Solr catch up
		if sync.numOps%sync.BatchSize == 0 {
			err := solr.Commit(sc)
			if err != nil {
				return err
			}
		}
	}

	// Commit to send the rest of the adds
	err = solr.Commit(sc)
	if err != nil {
		return err
	}

	return nil
}

// deleteDocuments will delete any documents from the Solr server that have
// been deleted from the MongoDB server.
//
// This function returns the number of operations completed and any possible
// error.
func deleteDocuments(mc *mongo.Config, sc *solr.Config, sync *Config) error {
	// Start progress bar, if in interactive mode
	var progress *progressbar.ProgressBar
	if sync.Interactive {
		progress = progressbar.Default(int64(sync.numSolr))
		fmt.Println("...looking for documents to delete")
	}

	for i := range sync.solrDocs {
		// Bump the progress bar
		if sync.Interactive {
			progress.Add(1)
		}

		sd := &sync.solrDocs[i]

		idx := sort.Search(sync.numMongo, func(n int) bool {
			return sync.mongoDocs[n].id >= sd.id
		})

		if idx == sync.numMongo || sync.mongoDocs[idx].id != sd.id {
			// Found in Solr, not found in Mongo; delete from Solr
			err := solr.DeleteDocument(sc, sd.id)
			if err != nil {
				return err
			}

			if sync.Verbose {
				fmt.Printf("   ...deleted document: %s\n", sd.id)
			}

			sync.numOps++
		}
	}

	// Commit to send the deletes
	err := solr.Commit(sc)
	if err != nil {
		return err
	}

	return nil
}

// Run will perform the MongoDB to Solr synchronization
func Run(mc *mongo.Config, sc *solr.Config, config ...Config) error {
	// Get our configuration
	sync := new(Config)
	if len(config) > 0 {
		sync.BatchSize = config[0].BatchSize
		sync.Force = config[0].Force
		sync.Interactive = config[0].Interactive
		sync.Verbose = config[0].Verbose
	}

	if sync.BatchSize == 0 {
		sync.BatchSize = 100
	}

	// Get all the documents from the two servers
	err := getDocuments(mc, sc, sync)
	if err != nil {
		return err
	}

	err = createDocuments(mc, sc, sync)
	if err != nil {
		return err
	}

	err = deleteDocuments(mc, sc, sync)
	if err != nil {
		return err
	}

	if sync.Interactive {
		fmt.Printf("...sync complete: %v sync operations performed\n", sync.numOps)
	}
	return nil
}

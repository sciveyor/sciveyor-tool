package sync

import (
	"errors"
	"fmt"

	"codeberg.org/sciveyor/sciveyor-tool/solr"

	"github.com/schollz/progressbar/v3"
	gosolr "github.com/vanng822/go-solr/solr"
)

// getSolrDocuments sets s.solrDocs to the full set of documents on the Solr
// server, expressed as document objects.
func getSolrDocuments(sc *solr.Config, sync *Config) ([]document, error) {
	// Count the number of documents, if we are in interactive mode
	var progress *progressbar.ProgressBar
	if sync.Interactive {
		numDocs, err := solr.Count(sc)
		if err != nil {
			// Can't get a count, so set the count to -1, which will make a progress
			// bar with just a spinner and no end
			numDocs = -1
		}

		fmt.Println("...fetching Solr collection")
		progress = progressbar.Default(int64(numDocs))
	}

	// Connect to Solr
	si, err := solr.Connect(sc)
	if err != nil {
		return nil, err
	}

	// Get all documents; just the three fields we want plus "type" so we can
	// skip author documents
	ret := []document{}

	query := gosolr.NewQuery()
	query.Q("*:*")
	query.FieldList("id,type,version,dataSourceVersion")
	query.AddParam("cursorMark", "*")
	query.Sort("id asc")
	query.Rows(1000)

	for {
		// Do the search
		se := si.Search(query)
		r, err := se.Result(nil)

		if err != nil {
			return nil, err
		}
		if r.Status != 0 {
			return nil, fmt.Errorf("expected successful Solr query, got status %v",
				r.Status)
		}

		for _, doc := range r.Results.Docs {
			// Bump the progress bar
			if sync.Interactive {
				progress.Add(1)
			}

			// Skip over author pseudo-documents
			if !doc.Has("type") {
				return nil, errors.New("Document is missing a type")
			}
			doctype, ok := doc.Get("type").(string)
			if !ok {
				return nil, errors.New("Document type is not a string")
			}

			if doctype != "article" {
				continue
			}

			// Extract the three values we need
			if !doc.Has("id") {
				return nil, errors.New("Document is missing an id")
			}
			id, ok := doc.Get("id").(string)
			if !ok {
				return nil, errors.New("Document id is not a string")
			}

			if !doc.Has("version") {
				return nil, errors.New("Document is missing a version")
			}
			version, ok := doc.Get("version").(float64)
			if !ok {
				return nil, errors.New("Document version is not a float")
			}

			if !doc.Has("dataSourceVersion") {
				return nil, errors.New("Document is missing a dataSourceVersion")
			}
			dataSourceVersion := doc.Get("dataSourceVersion").(float64)
			if !ok {
				return nil, errors.New("Document dataSourceVersion is not a float")
			}

			// Add a document record
			ret = append(ret,
				document{id, int(version), int(dataSourceVersion)})
		}

		// Are we done?
		if r.NextCursorMark == query.GetParam("cursorMark") {
			break
		}

		// Advance to the next cursor mark and keep searching
		query.RemoveParam("cursorMark")
		query.AddParam("cursorMark", r.NextCursorMark)
	}

	return ret, nil
}

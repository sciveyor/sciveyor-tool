package json

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"codeberg.org/sciveyor/sciveyor-tool/mongo"
	"codeberg.org/sciveyor/sciveyor-tool/transform"

	"github.com/bmatcuk/doublestar/v2"
	"github.com/schollz/progressbar/v3"
)

// ImportConfig specifies all optional parameters for Import.
type ImportConfig struct {
	BatchSize   int  // send to Mongo in batches of this size
	Interactive bool // if true, show progress bar
}

// Import takes the given list of files, loads them, transforms them to the
// format expected by MongoDB, and adds them to the MongoDB collection.
func Import(cfg *mongo.Config, files []string, config ...ImportConfig) error {
	// Get config
	batchSize := 100
	interactive := false

	if len(config) > 0 {
		if config[0].BatchSize != 0 {
			batchSize = config[0].BatchSize
		}
		interactive = config[0].Interactive
	}

	// Count the number of documents to add, if we are in interactive mode
	var progress *progressbar.ProgressBar
	if interactive {
		numDocs := int64(0)

		for _, pattern := range files {
			matches, err := doublestar.Glob(pattern)
			if err != nil {
				return fmt.Errorf("glob failed: %w", err)
			}

			numDocs += int64(len(matches))
		}

		progress = progressbar.Default(numDocs)
	}

	// Add the documents in batches
	var docsToAdd []interface{}
	addSize := 0

	for _, pattern := range files {
		matches, err := doublestar.Glob(pattern)
		if err != nil {
			return fmt.Errorf("glob failed: %w", err)
		}

		for _, filename := range matches {
			// Bump the progress bar
			if interactive {
				progress.Add(1)
			}

			// Read the file
			bytes, err := ioutil.ReadFile(filename)
			if err != nil {
				return fmt.Errorf("read failed for %s: %w", filename, err)
			}

			// Load the JSON
			var doc map[string]interface{}
			if err = json.Unmarshal(bytes, &doc); err != nil {
				return fmt.Errorf("unmarshal failed for %s: %w", filename, err)
			}

			// Transform the JSON
			if err = transform.JSONToMongo(doc); err != nil {
				return fmt.Errorf("json to mongo failed for %s: %w", filename, err)
			}

			// Add the document to the list
			docsToAdd = append(docsToAdd, doc)
			addSize++

			// Check if it's time to send
			if addSize%batchSize == 0 {
				err = mongo.CreateDocuments(cfg, docsToAdd)
				if err != nil {
					return fmt.Errorf("creating mongo doc failed for %s: %w", filename, err)
				}

				docsToAdd = nil
				addSize = 0
			}
		}
	}

	// Add any remaining documents
	if len(docsToAdd) > 0 {
		return mongo.CreateDocuments(cfg, docsToAdd)
	}

	return nil
}

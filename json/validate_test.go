package json

import (
	"log"
	"os"
	"testing"
)

func ExampleEmpty() {
	if err := ValidateFiles([]string{}); err != nil {
		log.Fatal(err)
	}

	// Output:
}

// Make sure to call os.Remove(f.Name()).
func makeSampleJSON(filename string, content string) (*os.File, error) {
	f, err := os.Create(filename)
	if err != nil {
		return nil, err
	}

	if _, err := f.Write([]byte(content)); err != nil {
		return nil, err
	}
	if err := f.Close(); err != nil {
		return nil, err
	}

	return f, nil
}

func TestBadJSON(t *testing.T) {
	f, err := makeSampleJSON("example.json", "badnotactuallyjson")
	if err != nil {
		t.Errorf("expected to make temporary file, got %v", err)
	}
	defer os.Remove(f.Name())

	if err := ValidateFiles([]string{f.Name()}); err == nil {
		t.Errorf("expected validating bad JSON not to work, but it did")
	}
}

func ExampleGood() {
	f, err := makeSampleJSON("example.json", `{
	"schema": "https://data.sciveyor.com/schema",
	"version": 5,
	"id": "thisisanid",
	"license": "some terms",
	"licenseUrl": "https://some.url.com/",
	"dataSource": "some place",
	"dataSourceUrl": "https://data.sciveyor.com/dataplace",
	"dataSourceVersion": 1,
	"type": "article"
}`)
	if err != nil {
		log.Fatal(err)
	}
	defer os.Remove(f.Name())

	if err := ValidateFiles([]string{f.Name()}); err != nil {
		log.Fatal(err)
	}
	// Output:
}

func ExampleBad() {
	f, err := makeSampleJSON("example.json", `{
	"schema": "https://data.sciveyor.com/schema",
	"version": "notaninteger",
	"id": "thisisanid",
	"license": "some terms",
	"licenseUrl": "https://some.url.com/",
	"dataSource": "some place",
	"dataSourceUrl": "https://data.sciveyor.com/dataplace",
	"dataSourceVersion": 1,
	"type": "article"
}`)
	if err != nil {
		log.Fatal(err)
	}
	defer os.Remove(f.Name())

	if err := ValidateFiles([]string{f.Name()}); err != nil {
		log.Fatal(err)
	}
	// Output:
	// example.json: /version: "notaninteger" type should be number, got string
	// example.json: /version: "notaninteger" must equal 5
}

func ExampleStrictFail() {
	f, err := makeSampleJSON("example.json", `{
	"schema": "https://data.sciveyor.com/schema",
	"version": 5,
	"id": "thisisanid",
	"license": "some terms",
	"licenseUrl": "https://some.url.com/",
	"dataSource": "some place",
	"dataSourceUrl": "https://data.sciveyor.com/dataplace",
	"dataSourceVersion": 1,
	"type": "article",
	"anextrafield": "this is not in the schema"
}`)
	if err != nil {
		log.Fatal(err)
	}
	defer os.Remove(f.Name())

	if err := ValidateFiles([]string{f.Name()}, ValidateConfig{Strict: true}); err != nil {
		log.Fatal(err)
	}
	// Output:
	// example.json: /: {"anextrafield":"thi... additional properties are not allowed
}

func ExampleUniqueFail() {
	f, err := makeSampleJSON("example.json", `{
	"schema": "https://data.sciveyor.com/schema",
	"version": 5,
	"id": "thisisanid",
	"license": "some terms",
	"licenseUrl": "https://some.url.com/",
	"dataSource": "some place",
	"dataSourceUrl": "https://data.sciveyor.com/dataplace",
	"dataSourceVersion": 1,
	"type": "article"
}`)
	if err != nil {
		log.Fatal(err)
	}
	defer os.Remove(f.Name())

	f2, err := makeSampleJSON("example2.json", `{
	"schema": "https://data.sciveyor.com/schema",
	"version": 5,
	"id": "thisisanid",
	"license": "some terms",
	"licenseUrl": "https://some.url.com/",
	"dataSource": "some place",
	"dataSourceUrl": "https://data.sciveyor.com/dataplace",
	"dataSourceVersion": 1,
	"type": "article"
}`)
	if err != nil {
		log.Fatal(err)
	}
	defer os.Remove(f2.Name())

	if err := ValidateFiles([]string{f.Name(), f2.Name()},
		ValidateConfig{Unique: true}); err != nil {
		log.Fatal(err)
	}
	// Output: [example.json example2.json]: non-unique id "thisisanid"
}

package json

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"codeberg.org/sciveyor/sciveyor-tool/util"
	"github.com/bmatcuk/doublestar/v2"
	"github.com/schollz/progressbar/v3"
)

// ValidateConfig specifies all optional parameters for Validate.
type ValidateConfig struct {
	Strict      bool // if true, don't allow extra fields in files
	Unique      bool // if true, check that all IDs are unique
	Interactive bool // if true, show progress bar
}

// ValidateFiles will validate the given set of JSON files.
func ValidateFiles(files []string, config ...ValidateConfig) error {
	// Get config
	strict := false
	unique := false
	interactive := false

	if len(config) > 0 {
		strict = config[0].Strict
		unique = config[0].Unique
		interactive = config[0].Interactive
	}

	// Load the schema
	schema, err := util.LoadSchema(strict)
	if err != nil {
		return fmt.Errorf("loading schema failed: %w", err)
	}

	// Count the number of documents to add, if we are in interactive mode
	var progress *progressbar.ProgressBar
	if interactive {
		numDocs := int64(0)

		for _, pattern := range files {
			matches, err := doublestar.Glob(pattern)
			if err != nil {
				return fmt.Errorf("glob failed: %w", err)
			}

			numDocs += int64(len(matches))
		}

		progress = progressbar.Default(numDocs)
	}

	// Save storage for tracking IDs, if we're in unique-check mode
	var ids map[string][]string = nil
	if unique {
		ids = map[string][]string{}
	}

	for _, pattern := range files {
		matches, err := doublestar.Glob(pattern)
		if err != nil {
			return fmt.Errorf("glob failed: %w", err)
		}

		for _, filename := range matches {
			// Bump the progress bar
			if interactive {
				progress.Add(1)
			}

			// Read the file
			bytes, err := ioutil.ReadFile(filename)
			if err != nil {
				return fmt.Errorf("read failed for %s: %w", filename, err)
			}

			// Validate it against the schema
			schemaErrs, err := schema.ValidateBytes(context.Background(), bytes)
			if err != nil {
				return fmt.Errorf("validate failed for %s: %w", filename, err)
			}

			if len(schemaErrs) > 0 {
				for _, se := range schemaErrs {
					fmt.Printf("%s: %s\n", filename, se.Error())
				}
			}

			// See if we're doing a uniqueness check. This is reasonably expensive (we
			// have to actually deserialize the JSON), which is why it's optional.
			//
			// Error modes in this check would have already been caught by the schema
			// validator, if they get raised, so we don't need to repeat ourselves
			// here.
			if unique {
				// Load the JSON
				var doc map[string]interface{}
				if err = json.Unmarshal(bytes, &doc); err != nil {
					continue
				}

				idRaw, ok := doc["id"]
				if !ok {
					continue
				}

				id, ok := idRaw.(string)
				if !ok {
					continue
				}

				arr, ok := ids[id]
				if ok {
					arr = append(arr, filename)
					ids[id] = arr
				} else {
					ids[id] = []string{filename}
				}
			}
		}
	}

	// Print all the uniqueness errors, if they exist
	if unique {
		for key, val := range ids {
			if len(val) > 1 {
				fmt.Printf("%v: non-unique id \"%v\"\n", val, key)
			}
		}
	}

	return nil
}

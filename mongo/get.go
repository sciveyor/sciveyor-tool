package mongo

import (
	"errors"

	"codeberg.org/sciveyor/sciveyor-tool/util"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// GetAllDocuments calls the given callback for every document in the database,
// possibly filtered by the given filter (usually a bson.D{}) and options.
//
// If the callback returns a non-nil error, further iteration will be aborted.
// There will be no filtering or transforms at all performed on the returned
// BSON document objects.
func GetAllDocuments(cfg *Config, filter interface{},
	opts *options.FindOptions, fn func(doc bson.M) error) error {
	// Connect to MongoDB
	coll, err := Connect(cfg)
	if err != nil {
		return err
	}
	defer Disconnect(cfg)

	// Perform the search
	cur, err := coll.Find(cfg.Context, filter, opts)
	if err != nil {
		return err
	}
	defer cur.Close(cfg.Context)

	// Loop the cursor and call the callback for each bson document
	for cur.Next(cfg.Context) {
		var result bson.M
		err := cur.Decode(&result)
		if err != nil {
			return err
		}

		err = fn(result)
		if err != nil {
			return err
		}
	}

	if err := cur.Err(); err != nil {
		return err
	}

	return nil
}

// GetDocument returns a single document, as a Go primitive. Note that the
// id value here is not the Mongo document ID, but the ID value of the
// document itself (i.e., the Sciveyor ID).
//
// There will be no filtering or transforms at all performed on the returned
// BSON document objects.
func GetDocument(cfg *Config, ps *util.PartialSchema, id string) (map[string]interface{}, error) {
	// Connect to MongoDB
	coll, err := Connect(cfg)
	if err != nil {
		return nil, err
	}
	defer Disconnect(cfg)

	// Use a cursor for safety, to make sure that there isn't more than one of
	// them (that would be bad)
	cur, err := coll.Find(cfg.Context, bson.D{bson.E{Key: "id", Value: id}})
	if err != nil {
		return nil, err
	}
	defer cur.Close(cfg.Context)

	doc := map[string]interface{}{}
	found := false
	for cur.Next(cfg.Context) {
		if found {
			return nil, errors.New("Found more than one document with id " + id)
		}
		found = true

		err := cur.Decode(&doc)
		if err != nil {
			return nil, err
		}
	}

	if err := cur.Err(); err != nil {
		return nil, err
	}

	return doc, nil
}

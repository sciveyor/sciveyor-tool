package mongo

import (
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// CreateDocuments creates documents, from a slice of Go primitives. The
// types in the primitives will be serialized according to the way in which Go
// types are automatically converted by Mongo's BSON library:
//
// https://pkg.go.dev/go.mongodb.org/mongo-driver/bson#hdr-Native_Go_Types
func CreateDocuments(cfg *Config, docs []interface{}) error {
	// Connect to MongoDB
	coll, err := Connect(cfg)
	if err != nil {
		return err
	}
	defer Disconnect(cfg)

	// This slows things down, but run a check to make sure that we don't
	// already have a document in the collection with that ID
	for _, docRaw := range docs {
		doc, ok := docRaw.(map[string]interface{})
		if !ok {
			return errors.New("Document is not a map")
		}

		idRaw, ok := doc["id"]
		if !ok {
			return errors.New("Document is missing an ID value")
		}

		id, ok := idRaw.(string)
		if !ok {
			return errors.New("Document ID is not a string")
		}

		count, err := coll.CountDocuments(cfg.Context,
			bson.D{bson.E{Key: "id", Value: id}},
			options.Count().SetLimit(1))
		if err != nil {
			return err
		}

		if count > 0 {
			return errors.New("Tried to add document with ID " + id +
				" that already exists")
		}
	}

	// Add the documents
	_, err = coll.InsertMany(cfg.Context, docs, nil)
	return err
}

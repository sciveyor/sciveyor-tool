package mongo

import (
	"context"
	"errors"
	"fmt"

	"codeberg.org/sciveyor/sciveyor-tool/util"

	"github.com/schollz/progressbar/v3"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// ValidateConfig specifies all optional parameters for Validate.
type ValidateConfig struct {
	Strict      bool // if true, don't allow extra fields in files
	Interactive bool // if true, show progress bar
}

// Validate will validate the contents of the MongoDB server against the schema.
func Validate(cfg *Config, config ...ValidateConfig) error {
	// Get config
	strict := true
	interactive := false

	if len(config) > 0 {
		strict = config[0].Strict
		interactive = config[0].Interactive
	}

	// Load the schema
	schema, err := util.LoadSchema(strict)
	if err != nil {
		return err
	}

	// Count the number of documents, if we are in interactive mode
	var progress *progressbar.ProgressBar
	if interactive {
		numDocs, err := Count(cfg, nil)
		if err != nil {
			// Can't get a count, so set the count to -1, which will make a progress
			// bar with just a spinner and no end
			numDocs = -1
		}

		progress = progressbar.Default(numDocs)
	}

	// Get all the documents
	err = GetAllDocuments(cfg, bson.D{}, nil,
		func(doc bson.M) error {
			// Bump the progress bar
			if interactive {
				progress.Add(1)
			}

			// Get Mongo's _id value out, and then delete it
			mongoID := ""
			for key, val := range doc {
				if key == "_id" {
					id, ok := val.(primitive.ObjectID)
					if !ok {
						return errors.New("Mongo ID not a primitive.ObjectID")
					}

					mongoID = id.String()
					break
				}
			}

			if mongoID == "" {
				return errors.New("Mongo ID missing")
			}

			delete(doc, "_id")

			// Validate the decoded instance directly
			vs := schema.Validate(context.Background(), doc)
			if len(*vs.Errs) > 0 {
				for _, se := range *vs.Errs {
					fmt.Printf("%v: %v\n", mongoID, se.Error())
				}
			}

			return nil
		})
	if err != nil {
		return err
	}

	return nil
}

package mongo

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Config encapsulates the list of parameters needed to connect to MongoDB.
type Config struct {
	Address    string
	Database   string
	Collection string
	Timeout    int

	// The context used to connect to this instance of MongoDB. Callers can use
	// this to make further MongoDB calls.
	Context context.Context
	cancel  context.CancelFunc

	client *mongo.Client
}

// Connect connects to MongoDB, given the connection parameters in this
// configuration.
func Connect(cfg *Config) (*mongo.Collection, error) {
	// Build a context
	ctx, cancel := context.WithTimeout(context.Background(),
		time.Duration(cfg.Timeout)*time.Second)

	// Connect to Mongo
	client, err := mongo.NewClient(options.Client().ApplyURI(cfg.Address))
	if err != nil {
		cancel()
		return nil, err
	}

	// Build a timeout context with our global timeout and use it for this entire
	// connection
	err = client.Connect(ctx)
	if err != nil {
		cancel()
		return nil, err
	}

	// Save the client
	cfg.Context = ctx
	cfg.cancel = cancel
	cfg.client = client

	// Return the collection requested
	return cfg.client.Database(cfg.Database).Collection(cfg.Collection), nil
}

// Disconnect disconnects from MongoDB. Whenever you call `mongo.Connect()`, you
// should immediately call `defer mongo.Disconnect()`.
func Disconnect(cfg *Config) {
	cfg.client.Disconnect(cfg.Context)
	cfg.cancel()
}

// Count counts the number of documents on the server, possibly filtered by the
// given filter.
func Count(cfg *Config, filter interface{}) (int64, error) {
	// Connect to MongoDB
	coll, err := Connect(cfg)
	if err != nil {
		return 0, err
	}
	defer Disconnect(cfg)

	// Get the count
	count, err := coll.CountDocuments(cfg.Context, filter)
	if err != nil {
		return 0, err
	}

	return count, nil
}
